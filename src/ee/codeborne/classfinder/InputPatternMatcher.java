package ee.codeborne.classfinder;

import java.util.*;

class InputPatternMatcher {
    private final static Collection<Character> specialCharacters = new HashSet<Character>() {{
        add('*');
        add(' ');
    }};

    private boolean isSpecialToken(String token) {
        return token.length() == 1 && specialCharacters.contains(token.charAt(0));
    }

    public boolean matches(String pattern, ClassFinder.ClassNamesEntry classNamesEntry) {
        List<String> pattenTokens = new InputPatternTokenizer(pattern).getTokens();
        String unmatchedString = classNamesEntry.getFullClassName();
        for (String token : pattenTokens) {
            if (!isSpecialToken(token)) {
                int positionOfMatch = unmatchedString.indexOf(token);
                if (positionOfMatch == -1) {
                    return false;
                }
                unmatchedString = unmatchedString.substring(positionOfMatch + token.length());
            }
        }

        boolean isEndReached = unmatchedString.isEmpty();
        String lastToken = pattenTokens.get(pattenTokens.size() - 1);
        return !(lastToken.equals(" ") && !isEndReached);
    }

    static class InputPatternTokenizer {

        private static final String PACKAGE_SEPARATOR = ".";
        String className;

        InputPatternTokenizer(String classNamePatten) {
            if (classNamePatten.contains(PACKAGE_SEPARATOR)) {
                int lastDotPosition = classNamePatten.lastIndexOf(PACKAGE_SEPARATOR);
                className = classNamePatten.substring(lastDotPosition + 1);
            } else {
                className = classNamePatten;
            }
        }

        public List<String> getTokens() {
            if (className.isEmpty()) {
                return Collections.emptyList();
            }

            List<String> tokens = new ArrayList<>();
            int lastTokenStartPosition = 0;
            for (int i = 1; i <= className.length(); i++) {
                boolean isLastChar = i == className.length();
                if (isLastChar || isTokenEndReached(i)) {
                    String token = className.substring(lastTokenStartPosition, i);
                    tokens.add(token);
                    lastTokenStartPosition = i;
                }
            }
            return tokens;
        }

        private boolean isTokenEndReached(int i) {
            char characterAtCurrentPosition = className.charAt(i);
            return Character.isUpperCase(characterAtCurrentPosition)
                    || specialCharacters.contains(characterAtCurrentPosition);
        }
    }
}



