package ee.codeborne.classfinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;

public class ClassFinder {
    private Collection<ClassNamesEntry> allClasses = new HashSet<>();
    private InputPatternMatcher inputPatternMatcher = new InputPatternMatcher();
    private BufferedReader classNamesReader;

    public ClassFinder(InputStream classNamesStream) {
        classNamesReader = new BufferedReader(new InputStreamReader(classNamesStream));
    }

    private void loadClassNamesFromStream() throws IOException {
        while (classNamesReader.ready()) {
            String line = classNamesReader.readLine();
            if (line != null) {
                ClassNamesEntry classNamesEntry = new ClassNamesEntry(line);
                allClasses.add(classNamesEntry);
            }
        }
    }

    public Collection<String> findMatching(String pattern) throws IOException {
        loadClassNamesFromStream();
        Collection<String> matching = new TreeSet<>();
        for (ClassNamesEntry entry : allClasses) {
            if (inputPatternMatcher.matches(pattern, entry)) {
                matching.add(entry.getFullClassName());
            }
        }
        return matching;
    }

    static class ClassNamesEntry implements Comparable<ClassNamesEntry> {

        private static final String PACKAGE_SEPARATOR = ".";
        private String className;
        private String fullClassName;

        ClassNamesEntry(String fullClassName) {
            this.fullClassName = fullClassName;
            if (fullClassName.contains(PACKAGE_SEPARATOR)) {
                int lastDotPosition = fullClassName.lastIndexOf(PACKAGE_SEPARATOR);
                className = fullClassName.substring(lastDotPosition + 1);
            } else {
                className = fullClassName;
            }
        }

        String getFullClassName() {
            return fullClassName;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || getClass() != o.getClass()) return false;
            ClassNamesEntry entry = (ClassNamesEntry) o;
            return className.equals(entry.className);
        }

        @Override
        public int compareTo(ClassNamesEntry entry) {
            return entry.className.compareTo(this.className);
        }

        @Override
        public int hashCode() {
            return className.hashCode();
        }
    }
}
