package ee.codeborne.classfinder;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class InputPatternMatcherTest {
    InputPatternMatcher inputPatternMatcher;

    @Before
    public void setUp(){
        inputPatternMatcher = new InputPatternMatcher();
    }

    @Test
    public void fullClassNameMatches(){
        assertTrue(inputPatternMatcher.matches("ClassOne", createClassNameEntry("ClassOne")));
    }

    private ClassFinder.ClassNamesEntry createClassNameEntry(String className) {
        return new ClassFinder.ClassNamesEntry(className);
    }

    @Test
    public void fullClassNameWithNoMatch(){
        assertFalse(inputPatternMatcher.matches("ClassTwo", createClassNameEntry("ClassOne")));
    }

    @Test
    public void partialMatch(){
        assertTrue(inputPatternMatcher.matches("Two", createClassNameEntry("ClassTwo")));
    }

    @Test
    public void asteriskMatch(){
        assertTrue(inputPatternMatcher.matches("Foo*Bar", createClassNameEntry("FooSomethingBar")));
    }

    @Test
    public void asteriskMatchWithLongName(){
        assertTrue(inputPatternMatcher.matches("Foo*Bar", createClassNameEntry("FooSomethingLongerBar")));
    }

    @Test
    public void zeroCharacterMatch(){
        assertTrue(inputPatternMatcher.matches("Foo*Bar", createClassNameEntry("FooBar")));
    }

    @Test
    public void twoWildcardsMatch(){
        assertTrue(inputPatternMatcher.matches("Foo*Bar*Foo", createClassNameEntry("FooSomethingBarRandomFoo")));
    }

    @Test
    public void startingWildCardMatch(){
        assertTrue(inputPatternMatcher.matches("*Bar", createClassNameEntry("FooBar")));
        assertFalse(inputPatternMatcher.matches("*Bar", createClassNameEntry("ClassFoo")));
    }

    @Test
    public void endingWildCardMatch(){
        assertTrue(inputPatternMatcher.matches("Foo*", createClassNameEntry("FooBar")));
        assertFalse(inputPatternMatcher.matches("Bar*", createClassNameEntry("Foo")));
        assertTrue(inputPatternMatcher.matches("*Foo*", createClassNameEntry("XFooX")));
    }

    @Test
    public void onlyAsteriskMatchesEveryString(){
        assertTrue(inputPatternMatcher.matches("*", createClassNameEntry("ClassOne")));
        assertTrue(inputPatternMatcher.matches("*", createClassNameEntry("java")));
        assertTrue(inputPatternMatcher.matches("*", createClassNameEntry("")));
        assertTrue(inputPatternMatcher.matches("*", createClassNameEntry("ee.codeborne.JeeJee")));
    }
}
