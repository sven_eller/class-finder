package ee.codeborne.classfinder;

import org.junit.Test;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class ClassFinderTest {

    @Test
    public void findMatchingByCamelCaseCapitals() throws IOException {
        String searchPattern = "FB";
        Collection<String> result = findMatching(searchPattern, "a.b.FooBarBaz\nc.d.FooBar");
        assertThat(result, hasItem("a.b.FooBarBaz"));
        assertThat(result, hasItem("c.d.FooBar"));
    }

    @Test
    public void findMatchingByAsterisk() throws IOException {

        String searchPattern = "F*Baz";
        String inputClasses = "a.b.FooBarBaz\nc.d.FooBar";
        Collection<String> result = findMatching(searchPattern, inputClasses);

        assertThat(result, hasItem("a.b.FooBarBaz"));
        assertThat(result, not(hasItem("c.d.FooBar")));
    }

    @Test
    public void findMatchingByAsteriskAndInitials() throws IOException {
        String searchPattern = "*BB";
        String inputClasses = "a.b.FooBarBaz\nc.d.FooBar";
        Collection<String> result = findMatching(searchPattern, inputClasses);
        assertThat(result, hasItem("a.b.FooBarBaz"));
        assertThat(result, not(hasItem("c.d.FooBar")));
    }

    @Test
    public void findMatchingByFullNameWithPackage() throws IOException {
        String searchPattern = "ee.codeborne.ClassOne";
        String inputClasses = "ee.codeborne.ClassOne\na.b.ClassOne";
        Collection<String> result = findMatching(searchPattern, inputClasses);
        assertThat(result, hasItem(searchPattern));
        assertThat(result, not(hasItem("a.b.ClassOne")));
    }

    @Test
    public void findMatchingByClassName() throws IOException {
        Collection<String> result = findMatching("ClassOne", "ee.codeborne.ClassOne");
        assertThat(result, hasItem("ee.codeborne.ClassOne"));
    }

    @Test
    public void findMatchingEndingWithSpace() throws IOException {
        String searchPattern = "ClassOne ";
        Collection<String> result = findMatching(searchPattern, "ee.codeborne.ClassOne\nee.codeborne.ClassOneMore");
        assertThat(result, hasItem("ee.codeborne.ClassOne"));
        assertThat(result, not(hasItem("ee.codeborne.ClassOneMore")));
    }

    @Test
    public void findMatchingByCamelCaseCapitalsSeparatedByAsterisk() throws IOException {
        String searchPattern = "C*M";
        Collection<String> result = findMatching(searchPattern, "ee.codeborne.ClassOne\nee.codeborne.ClassOneMore");
        assertThat(result, hasItem("ee.codeborne.ClassOneMore"));
        assertThat(result, not(hasItem("ee.codeborne.ClassOne")));
    }

    @Test
    public void resultIsSortedByClassName() throws IOException {
        ClassFinder classFinder = createClassFinder("ee.codeborne.Xyx\nee.codeborne.Cbx\n" + "ee.codeborne.Abc");
        Collection<String> result = classFinder.findMatching("*");
        Iterator<String> it = result.iterator();
        assertThat(it.next(), equalTo("ee.codeborne.Abc"));
        assertThat(it.next(), equalTo("ee.codeborne.Cbx"));
        assertThat(it.next(), equalTo("ee.codeborne.Xyx"));
    }

    private ClassFinder createClassFinder(String className) throws IOException {
        return new ClassFinder(getInputStreamFromString(className));
    }

    private InputStream getInputStreamFromString(String inputString) {
        return new ByteArrayInputStream(inputString.getBytes());
    }

    private Collection<String> findMatching(String searchPattern, String inputString) throws IOException {
        ClassFinder classFinder = createClassFinder(inputString);
        return classFinder.findMatching(searchPattern);
    }

}
