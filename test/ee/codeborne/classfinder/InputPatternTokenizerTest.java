package ee.codeborne.classfinder;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.internal.matchers.IsCollectionContaining.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class InputPatternTokenizerTest {
    @Test
    public void tokenizeClassInitialsInput() {
        Collection<String> tokens = tokenize("ABC");
        assertThat(tokens, hasItems("A", "B", "C"));
    }

    @Test
    public void tokenizeClassName() {
        Collection<String> tokens = tokenize("OneClass");
        assertThat(tokens, hasItems("Class", "One"));
    }

    @Test
    public void tokenizeWhiteSpaceEndingPattern() {
        Collection<String> tokens = tokenize("OneClass ");
        assertThat(tokens, hasItems("Class", "One", " "));
    }

    @Test
    public void tokenizeWithAsterisk() {
        List<String> tokens = tokenize("One*Class");
        assertThat(tokens, is(Arrays.asList("One", "*", "Class")));
    }

    @Test
    public void tokenizeWithMultipleAsterisk() {
        List<String> tokens = tokenize("One*Name*Multiple*Asterisk ");
        assertThat(tokens, is(Arrays.asList("One", "*", "Name", "*", "Multiple", "*", "Asterisk", " ")));
    }

    @Test
    public void tokenizeWithPackageName() {
        List<String> tokens = tokenize("ee.codeborne.classfinder.One*Name*Multiple*Asterisk ");
        assertThat(tokens, is(Arrays.asList("One", "*", "Name", "*", "Multiple", "*", "Asterisk", " ")));
    }

    @Test
    public void tokenizeEmptyString() {
        InputPatternMatcher.InputPatternTokenizer inputPatternTokenizer = new InputPatternMatcher.InputPatternTokenizer("");
        Collection<String> classNameTokens = inputPatternTokenizer.getTokens();
        assertThat(classNameTokens.size(), equalTo(0));
    }

    @Test(expected = NullPointerException.class)
    public void tokenizeNull() {
        new InputPatternMatcher.InputPatternTokenizer(null);
    }

    @Test
    public void tokenizeLowercaseString() {
        Collection<String> classNameTokens = tokenize("classone");
        assertThat(classNameTokens, hasItem("classone"));
        assertThat(classNameTokens.size(), equalTo(1));
    }

    private List<String> tokenize(String stringToTokenize) {
        InputPatternMatcher.InputPatternTokenizer patternTokenizer = new InputPatternMatcher.InputPatternTokenizer(stringToTokenize);
        return patternTokenizer.getTokens();
    }

}
